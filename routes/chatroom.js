
/*
 * GET chatroom page.
 */

exports.chatroom = function(req, res){
  var username = req.param('username');
  res.render('chatroom', { username: username });
};
