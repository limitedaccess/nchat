var socketio = require('socket.io');

function startChatServer(server){
    var io = socketio.listen(server);
    io.sockets.on('connection', function (socket) {
    socket.emit('news', { hello: 'world' });

    socket.broadcast.send("someone is connected");
    socket.on('chat', function (data) {
            socket.emit('message', 'you said: ' + data.message)
            socket.broadcast.emit('message', data.sender + " said: "+ data.message);
        });
    });
}

exports.startChatServer = startChatServer;
