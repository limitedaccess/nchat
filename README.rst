Working Environment
===================
You can set up node.js and npm environment for your machine, 
or create a virtual node.js environment if you're using python virtualenv already

Here are the steps:
-------------------
Run these commands in your terminal::

    $ mkvirtualenv <name>
    $ pip install nodeenv 
    $ nodeenv env
    $ . env/bin/activate

Then run::
    $ npm install
