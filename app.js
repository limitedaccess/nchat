
/**
 * Module dependencies.
 */

var express = require('express')
  , cons = require('consolidate')
  , swig = require('swig')
  , routes = require('./routes')
  , user = require('./routes/user')
  , chatroom = require('./routes/chatroom')
  , http = require('http')
  , path = require('path')
  , chatserv = require('./chatserv');

swig.Swig({
  allowErrors: false,
  autoescape: true,
  encoding: 'utf8',
  filters: {},
  root: __dirname + '/views',
  tags: {},
  extensions: {},
  tzOffset: 0
});

var app = express();
app.engine('html', cons.swig);

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('view engine', 'html');
  app.set('views', __dirname + '/views');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser('your secret here'));
  app.use(express.session());
  app.use(app.router);
  app.use(require('stylus').middleware(__dirname + '/public'));
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

var users = [{name:'hugh', address:'localhost'}];

app.get('/', routes.index);
app.get('/users', user.list);
app.get('/chatroom', chatroom.chatroom);

app.post('/', function(req, res){
    var username = req.param('username');
    users.push({name: username, addres: req.connection.remoteAddress});
    res.redirect('/chatroom/?username=' + username);
});

app.locals({
    users: users
});
var server = http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
/* start socket.io process */
chatserv.startChatServer(server);
